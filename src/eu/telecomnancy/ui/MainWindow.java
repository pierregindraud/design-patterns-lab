package eu.telecomnancy.ui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.WindowConstants;

import eu.telecomnancy.helpers.ReadPropertyFile;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.command.Invoker;
import eu.telecomnancy.sensor.command.iCommand;

public class MainWindow extends JFrame {

    private ISensor sensor;
    private SensorView sensorView;
    
    private JMenuBar menubar = new JMenuBar();
    private JMenu menu_commande = new JMenu("Commandes");
    
    private Invoker invoker;
    
    private List<iCommand> lst_com = new ArrayList<iCommand>();

    public MainWindow(ISensor sensor) {
        this.sensor = sensor;
        this.sensorView = new SensorView(this.sensor);
        
        invoker = new Invoker();
        
        // BUILD THE MENU BAR
        this.setJMenuBar(menubar);
        menubar.add(menu_commande);
        
        // LOAD THE ITEM IN COMMAND MENU
        ReadPropertyFile rp=new ReadPropertyFile();
		try {
			Properties p = rp.readFile("/eu/telecomnancy/commande.properties");
			// loop over each menu item
			for (String key : p.stringPropertyNames()) {
				try {
					Class<?> cls = Class.forName((String)(p.getProperty(key)));
					final iCommand com = (iCommand)cls.getConstructor(ISensor.class).newInstance(sensor);
					lst_com.add(com);
					JMenuItem it = new JMenuItem(key);
					it.addActionListener(new ActionListener() {
			            @Override
			            public void actionPerformed(ActionEvent e) {
			            	try {
								invoker.executeCommand(com);
							} catch (Exception e1) {
								// No exception are catched
								e1.printStackTrace();
							}
			            }
			        });
					menu_commande.add(it);
				} catch (Exception e) {
					System.err.println("Unable to load " + key + " class : "+ e.toString());
					continue;
				}
	        }
		} catch (IOException e) {
			e.printStackTrace();
		}
        
        this.setLayout(new BorderLayout());
        this.add(this.sensorView, BorderLayout.CENTER);

        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.pack();
        this.setVisible(true);
    }


}
