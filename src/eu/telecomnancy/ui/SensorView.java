package eu.telecomnancy.ui;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;
import eu.telecomnancy.sensor.command.CommandeGetValue;
import eu.telecomnancy.sensor.command.CommandeOff;
import eu.telecomnancy.sensor.command.CommandeOn;
import eu.telecomnancy.sensor.command.CommandeUpdate;
import eu.telecomnancy.sensor.command.Invoker;
import eu.telecomnancy.sensor.command.iCommand;

public class SensorView extends JPanel implements Observer {
    private ISensor sensor;

    private JLabel value = new JLabel("N/A °C");
    private JButton on = new JButton("On");
    private JButton off = new JButton("Off");
    private JButton update = new JButton("Acquire");
    
    private iCommand c_on;
    private iCommand c_off;
    private iCommand c_update;
    private iCommand c_get_value;
    
    private Invoker invoker;

    public SensorView(ISensor c) {
        this.sensor = c;
        this.setLayout(new BorderLayout());

        // add a link to the observer into the observable
        ((Observable)c).addObserver(this);
        
        // registered default commands
        c_on = new CommandeOn(c);
        c_off = new CommandeOff(c);
        c_update = new CommandeUpdate(c);
        c_get_value = new CommandeGetValue(c);
        
        invoker = new Invoker();

        value.setHorizontalAlignment(SwingConstants.CENTER);
        Font sensorValueFont = new Font("Sans Serif", Font.BOLD, 18);
        value.setFont(sensorValueFont);
        
        
        this.add(value, BorderLayout.CENTER);


        on.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            	try {
					invoker.executeCommand(c_on);
				} catch (Exception e1) {
					// No exception are catched
					e1.printStackTrace();
				}
            }
        });

        off.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            	try {
					invoker.executeCommand(c_off);
				} catch (Exception e1) {
					// No exception are catched
					e1.printStackTrace();
				}
            }
        });

        update.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    invoker.executeCommand(c_update);
                } catch (SensorNotActivatedException sensorNotActivatedException) {
                    sensorNotActivatedException.printStackTrace();
                } catch (Exception error) {
                    error.printStackTrace();
                }
            }
        });

        JPanel buttonsPanel = new JPanel();
        buttonsPanel.setLayout(new GridLayout(1, 3));
        buttonsPanel.add(update);
        buttonsPanel.add(on);
        buttonsPanel.add(off);

        this.add(buttonsPanel, BorderLayout.SOUTH);
    }

    @Override
	public void update(Observable arg0, Object arg1) {    
    	try {
    		double val = (double)invoker.executeCommand(c_get_value);
			this.value.setText(val + " °C");
    	} catch (SensorNotActivatedException sensorNotActivatedException) {
    		sensorNotActivatedException.printStackTrace();
    	} catch (Exception error) {
    		error.printStackTrace();
    	}
    }
}
