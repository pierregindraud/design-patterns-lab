package eu.telecomnancy;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.factory.AppSensorFactory;
import eu.telecomnancy.sensor.factory.iSensorFactory;
import eu.telecomnancy.ui.MainWindow;

public class SwingApp {

    public static void main(String[] args) {
    	iSensorFactory factory = new AppSensorFactory();
    	ISensor sensor = null;
    	
    	try {
			sensor = factory.createSensor();
		} catch (Exception e) {
			System.err.println(e.getMessage());
			return;
		}
    	
        new MainWindow(sensor);
    }

}
