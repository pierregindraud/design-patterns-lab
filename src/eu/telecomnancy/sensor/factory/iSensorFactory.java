package eu.telecomnancy.sensor.factory;

import java.io.IOException;

import eu.telecomnancy.sensor.ISensor;

public interface iSensorFactory {
	public ISensor createSensor() throws Exception;
}
