package eu.telecomnancy.sensor.factory;

import java.util.Properties;

import eu.telecomnancy.helpers.ReadPropertyFile;
import eu.telecomnancy.sensor.ISensor;

public class AppSensorFactory implements iSensorFactory {

	public ISensor createSensor() throws Exception {
		ReadPropertyFile rp = new ReadPropertyFile();
		
        Properties p;
		p = rp.readFile("/eu/telecomnancy/app.properties");
		
		// check key presence
		if ( ! p.containsKey("factory") ) {
			throw new Exception("'factory' properties not defined");
		}
		
		// check value type
		if ( ! p.get("factory").getClass().equals(String.class)) {
			throw new Exception("'factory' properties doesn't seem to be a string");
		}
		String class_path = (String)p.get("factory");
		
		// retrieve the Class type definition
		Class<?> cls = Class.forName(class_path);
		Object instance = cls.newInstance();
		
		if ( ! (instance instanceof ISensor) ) {
			throw new Exception("Class " + class_path + " does not implement the ISensor interface");
		}
		
		return (ISensor)instance;
	}
	
}
