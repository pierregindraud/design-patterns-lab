package eu.telecomnancy.sensor;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: charoy
 * Date: 13/12/13
 * Time: 18:16
 */
public class SimpleSensorLogger implements SensorLogger {
    @Override
    public void log(LogLevel level, String message) {
    	SimpleDateFormat sd = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    	Date date = new Date();
        System.out.println(sd.format(date) + " " + level.name() + " " + message);
    }
}
