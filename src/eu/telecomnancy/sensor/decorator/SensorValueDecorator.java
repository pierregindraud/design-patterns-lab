package eu.telecomnancy.sensor.decorator;

import java.util.Observable;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

public abstract class SensorValueDecorator extends Observable implements ISensor {

	// register a link to the decorated object
	protected ISensor decorated;
	
	public SensorValueDecorator(ISensor decorated) {
		this.decorated = decorated;
	}
	
	// ## Redirect observer call to preserve GUI behaviour 
    @Override
    public synchronized void addObserver(java.util.Observer o) {
    	((Observable)this.decorated).addObserver(o);
    };
    // ## 
	
	// capture the getValue() call
	public double getValue() throws SensorNotActivatedException {
		return decorated.getValue();
	}
	
	// OVERRIDE ALL ISensors methods
	@Override
	public void on() {
		this.decorated.on();
	}

	@Override
	public void off() {
		this.decorated.off();
		
	}

	@Override
	public boolean getStatus() {
		return this.decorated.getStatus();
	}

	@Override
	public void update() throws SensorNotActivatedException {
		this.decorated.update();
	}
}
