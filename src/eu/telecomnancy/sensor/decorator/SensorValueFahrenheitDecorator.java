package eu.telecomnancy.sensor.decorator;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

public class SensorValueFahrenheitDecorator extends SensorValueDecorator {

	// CONSTRUCTOR
	public SensorValueFahrenheitDecorator(ISensor decorated) {
		super(decorated);
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		return this.decorated.getValue() * 1.8 + 32;
	}
}
