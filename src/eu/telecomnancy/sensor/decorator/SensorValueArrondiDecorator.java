package eu.telecomnancy.sensor.decorator;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

public class SensorValueArrondiDecorator extends SensorValueDecorator {

	public SensorValueArrondiDecorator(ISensor decorated) {
		super(decorated);
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		return Math.round(this.decorated.getValue());
	}
}
