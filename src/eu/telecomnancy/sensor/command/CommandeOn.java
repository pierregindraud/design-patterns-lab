package eu.telecomnancy.sensor.command;

import eu.telecomnancy.sensor.ISensor;

public class CommandeOn implements iCommand {

	private ISensor receiver;

	// CONSTRUCTOR
	public CommandeOn(ISensor receive) {
		this.receiver = receive;
	}
	
	@Override
	public Object execute() throws Exception {
		receiver.on();
		return null;
	}

}
