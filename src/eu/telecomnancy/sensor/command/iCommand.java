package eu.telecomnancy.sensor.command;

public interface iCommand {
	public Object execute() throws Exception;
}
