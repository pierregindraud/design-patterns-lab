package eu.telecomnancy.sensor.command;

import eu.telecomnancy.sensor.ISensor;

public class CommandeGetValue implements iCommand {

	private ISensor receiver;

	// CONSTRUCTOR
	public CommandeGetValue(ISensor receive) {
		this.receiver = receive;
	}
	
	@Override
	public Object execute() throws Exception {
		return receiver.getValue();
	}

}
