package eu.telecomnancy.sensor.command;

public class Invoker {

	public Object executeCommand(iCommand cmd) throws Exception {
		return cmd.execute();
	}
	
}
