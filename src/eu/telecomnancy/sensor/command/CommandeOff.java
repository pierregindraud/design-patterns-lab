package eu.telecomnancy.sensor.command;

import eu.telecomnancy.sensor.ISensor;

public class CommandeOff implements iCommand {

	private ISensor receiver;

	// CONSTRUCTOR
	public CommandeOff(ISensor receive) {
		this.receiver = receive;
	}
	
	@Override
	public Object execute() throws Exception {
		receiver.off();
		return null;
	}

}
