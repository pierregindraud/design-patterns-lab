package eu.telecomnancy.sensor.command;

import eu.telecomnancy.sensor.ISensor;

public class CommandeUpdate implements iCommand {

	private ISensor receiver;

	// CONSTRUCTOR
	public CommandeUpdate(ISensor receive) {
		this.receiver = receive;
	}
	
	@Override
	public Object execute() throws Exception{
		receiver.update();
		return null;
	}

}
