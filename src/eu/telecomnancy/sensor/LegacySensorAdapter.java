package eu.telecomnancy.sensor;

public class LegacySensorAdapter extends LegacyTemperatureSensor implements ISensor {

	@Override
	public void on() {
		// if off turn on
		if (!getStatus()) {
			super.onOff();
		}
	}

	@Override
	public void off() {
		// if on turn off
		if (getStatus()) {
			super.onOff();
		}
	}

	@Override
	public boolean getStatus() {
		// return the new status
		return super.getStatus();
	}

	@Override
	public void update() throws SensorNotActivatedException {
		if (!getStatus()) {
			throw new SensorNotActivatedException("Le sensors n'est pas activé");
		}
		
		if(this.getStatus()) {
			// stop
			super.onOff();
		}
		
		// start to acquire a new value
		super.onOff();
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		if (!getStatus()) {
			throw new SensorNotActivatedException("Le sensors n'est pas activé");
		}
		return super.getTemperature();
	}

}
