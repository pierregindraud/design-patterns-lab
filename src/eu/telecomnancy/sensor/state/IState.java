package eu.telecomnancy.sensor.state;

interface IState {
	// Try to set the state to ON 
	public void turnOn(SensorContext c);

	// Try to set the state to OFF
	public void turnOff(SensorContext c);
	
	// return the state status
	public boolean getStatus();
}
