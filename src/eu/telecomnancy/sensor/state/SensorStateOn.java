package eu.telecomnancy.sensor.state;

public class SensorStateOn implements IState {
	
	@Override
	public void turnOn(SensorContext c) {
		System.out.println("Already ON");
	}

	@Override
	public void turnOff(SensorContext c) {
		c.changeState(new SensorStateOff());
	}

	@Override
	public boolean getStatus() {
		return true;
	}

}
