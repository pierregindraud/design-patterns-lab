package eu.telecomnancy.sensor.state;

public class SensorContext {
	// current state of context
	IState current_state;
	
	// Default constructor
	public SensorContext() {
		// init context to OFF
		this.current_state = new SensorStateOff();
	}
	
	//callback to allow state to change the context current state
	protected void changeState(IState new_state) {
		this.current_state = new_state;
	}
	
	// Try to set the state to ON 
	public void turnOn() {
		this.current_state.turnOn(this);
	};

	// Try to set the state to OFF
	public void turnOff() {
		this.current_state.turnOff(this);
	};
	
	public boolean getStatus() {
		return this.current_state.getStatus();
	}
}
