package eu.telecomnancy.sensor.state;

class SensorStateOff implements IState {

	@Override
	public void turnOn(SensorContext c) {
		c.changeState(new SensorStateOn());
	}

	@Override
	public void turnOff(SensorContext c) {
		System.out.println("Already OFF");
	}

	@Override
	public boolean getStatus() {
		return false;
	}

}
