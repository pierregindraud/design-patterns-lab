package eu.telecomnancy.sensor;

import java.util.Observable;

public class ProxyTemperatureSensor extends Observable implements ISensor {
    
	private SensorLogger logger;
	private ISensor real_sensor;
	
    public ProxyTemperatureSensor(ISensor s) {
    	 this.logger = new SimpleSensorLogger();
    	 this.real_sensor = s;
	}
    
    // ## Redirect observer call to preserve GUI behaviour 
    @Override
    public synchronized void addObserver(java.util.Observer o) {
    	((Observable)this.real_sensor).addObserver(o);
    };
    // ## 
    
    @Override
    public void on() {
    	this.logger.log(LogLevel.INFO, "Turn on");
    	this.real_sensor.on();
    }

    @Override
    public void off() {
    	this.logger.log(LogLevel.INFO, "Turning off");
    	this.real_sensor.off();
    }

    @Override
    public boolean getStatus() {
    	boolean state = this.real_sensor.getStatus();
    	this.logger.log(LogLevel.INFO, "Perform status get : " + state);
    	return state;
    }

    @Override
    public void update() throws SensorNotActivatedException {
    	this.logger.log(LogLevel.INFO, "Perform update");
        this.real_sensor.update();
    }

    @Override
    public double getValue() throws SensorNotActivatedException {
        double val = this.real_sensor.getValue(); 
        this.logger.log(LogLevel.INFO, "Perform value  get : " + val);
    	return val;
    }

}
