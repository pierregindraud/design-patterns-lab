package eu.telecomnancy.sensor;

import java.util.Observable;
import java.util.Random;

import eu.telecomnancy.sensor.state.SensorContext;

public class TemperatureSensorV2 extends Observable implements ISensor {
    double value = 0;
    
    // keep a link to the private automate context
    SensorContext automate;
    
    public TemperatureSensorV2() {
		this.automate = new SensorContext();
	}
    
    @Override
    public void on() {
        this.automate.turnOn();
    }

    @Override
    public void off() {
        this.automate.turnOff();
    }

    @Override
    public boolean getStatus() {
        return this.automate.getStatus();
    }

    @Override
    public void update() throws SensorNotActivatedException {
        if (this.getStatus()) {
            value = (new Random()).nextDouble() * 100;
            this.setChanged();
            this.notifyObservers();
        }
        else throw new SensorNotActivatedException("Sensor must be activated before acquiring new values.");
    }

    @Override
    public double getValue() throws SensorNotActivatedException {
    	if (this.getStatus())
            return value;
        else throw new SensorNotActivatedException("Sensor must be activated to get its value.");
    }

}
